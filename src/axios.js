// axios
import axios from 'axios'
import { secretKey } from '@/auth/secretKey'
import router from '@/router'

const baseURL = 'https://api.learning.icommits.co.id'

const instance = axios.create({
  baseURL,
  headers: {
    secretKey
  }
})

// Add Interceptors to response
instance.interceptors.response.use(function (response) {
  return response
}, function (error) {
  if (error.response.status === 401) {
    sessionStorage.clear()
    localStorage.clear()
    router.push({ path: '/login' })
  }
  return Promise.reject(error)
})

export default instance
