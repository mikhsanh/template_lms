export default [
  {
    url: '/dashboard',
    name: 'Dashboard',
    icon: 'ActivityIcon',
    i18n: 'Dashboard',
    isLogin: true
  },
  // {
  //   url: '/data-pendaftaran',
  //   name: 'Biodata',
  //   icon: 'UserIcon',
  //   i18n: 'Biodata',
  //   isLogin: true
  // },
  {
    url: '/home',
    name: 'Home',
    icon: 'HomeIcon',
    i18n: 'Home'
  },
  {
    url: '/daftar',
    name: 'Daftar',
    icon: 'PlusIcon',
    i18n: 'Daftar'
  },
  {
    url: '/login',
    name: 'Login',
    icon: 'LogInIcon',
    i18n: 'Login'
  },

  {
    header: 'Pembelajaran',
    icon: 'PackageIcon',
    i18n: 'Pembelajaran',
    isLogin: true,
    items: [
      {
        url: '/materi',
        name: 'Materi',
        slug: 'materi',
        icon: 'BookOpenIcon',
        i18n: 'Materi',
        isLogin: true
      },
      {
        url: '/tugas',
        name: 'Tugas',
        slug: 'tugas',
        icon: 'ClipboardIcon',
        i18n: 'Tugas',
        isLogin: true
      },
      // {
      //   url: '/btq',
      //   name: "Hafalan Qur'an",
      //   slug: "Hafalan Qur'an",
      //   icon: 'BookIcon',
      //   i18n: "Hafalan Qur'an",
      //   isLogin: true
      // },
      {
        url: '/ujian',
        name: 'Ujian',
        slug: 'ujian',
        icon: 'ColumnsIcon',
        i18n: 'Ujian',
        isLogin: true
      },
      {
        url: null,
        name: 'Jadwal',
        tag: '2',
        tagColor: 'primary',
        icon: 'CalendarIcon',
        i18n: 'Jadwal',
        isLogin: true,
        submenu: [
          {
            url: '/jadwal/pembelajaran',
            name: 'Pembelajaran',
            slug: 'jadwal-pembelajaran',
            i18n: 'Pembelajaran',
            isLogin: true
          },
          {
            url: '/jadwal/akademik',
            name: 'Akademik',
            slug: 'jadwal-akademik',
            i18n: 'Akademik',
            isLogin: true
          }
        ]
      }
      // {
      //   url: '/raport',
      //   name: 'Raport',
      //   slug: 'raport',
      //   icon: 'FileTextIcon',
      //   i18n: 'Raport',
      //   isLogin: true
      // }
    ]
  },
  {
    header: 'Info Lainnya',
    icon: 'PackageIcon',
    i18n: 'Info Lainnya',
    isLogin: true,
    items: [
      {
        url: '/e-guide',
        name: 'E-Guide',
        slug: 'e-guide',
        icon: 'ActivityIcon',
        i18n: 'E-Guide',
        isLogin: true
      },
      {
        url: '/mading',
        name: 'mading',
        slug: 'mading',
        icon: 'GridIcon',
        i18n: 'Mading',
        isLogin: true
      }
    ]
  }
]

