import optionsAPI from '@/http/requests/options'

export default {
  domainSiswa () {
    const data = {
      api: 'first',
      url: 'auth/aplikasi',
      method: 'GET',
      params:  {
        domain_siswa: window.location.origin
      }
    }
    return optionsAPI(data)
  }
}
