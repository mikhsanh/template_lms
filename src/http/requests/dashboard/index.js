// import banner from '@/http/requests/others/banner'
import jadwal from '@/http/requests/jadwal'
import materi from '@/http/requests/materi'
import tugas from '@/http/requests/tugas'
import ujian from '@/http/requests/ujian'
import pengajar from '@/http/requests/others/pengajar'

export default {
  async getDashboard () {

    const getMapel = await pengajar.getMapelId().then(response => {
      return response
    }).catch(err => {
      console.log(err)
    })

    // UJIAN
    const dataTypeUjian = await ujian.getUjian(getMapel).then(response => {
      return response.data
    }).catch(err => {
      console.log(err)
    })

    let count = 0
    const dataUjian = await Promise.all(dataTypeUjian.map(async item => {
      const listUjian = await ujian.getSoalUjian(item.id)
      count = +listUjian.data.length
      return count
    }))


    const countUjian = dataUjian.reduce((item, x) => {
      return item + x
    })
    // End Of get Count Ujian
    
    // Materi
    const dataMateri = await materi.getMateri().then(response => {
      return response.data.length
    }).catch(err => {
      console.log(err.response)
    })
    // End of Get Count Materi

    // Tugas
    const dataTugas = await tugas.getTugas().then(response => {
      return response.data.length
    }).catch(err => {
      console.log(err.response)
    })
    // End of Get Count Tugas

    // Jadwal
    const dataJadwal = await jadwal.getJadwal(getMapel).then(response => {
      return response.data
    })
    
    let dateNow = new Date()
    dateNow = `${dateNow.getFullYear()}-${dateNow.getMonth() + 1}-${dateNow.getDate()}`
    dateNow = new Date(dateNow)

    const todayJadwal = dataJadwal.filter(item => {
      return new Date(item.mulai.substr(0, 10)).getTime() === dateNow.getTime() || new Date(item.selesai.substr(0, 10)).getTime() > dateNow.getTime()
    })
    // Get Today Jadwal
    
    const dataDashboard = {
      dashboard: [
        // {icon: 'BookIcon', statistic: 0, statisticTitle: 'Hafalan & Bacaan Quran', progress: 0},
        {icon: 'BookOpenIcon', statistic: dataMateri, statisticTitle: 'Materi', progress: 0},
        {icon: 'ClipboardIcon', statistic: dataTugas, statisticTitle: 'Tugas', progress: 0},
        {icon: 'AwardIcon', statistic: countUjian, statisticTitle: 'Ujian', progress: 0}
      ],
      jadwal: todayJadwal
    }

    return new Promise((resolve) => {
      resolve(dataDashboard)
    })
  }
}

