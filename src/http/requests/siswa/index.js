import optionsAPI from '@/http/requests/options'
import store from '@/store/store'

const dataPendaftaran =  {
  id: 5,
  sekolah_id: 3,
  email: 'dimassetiaji30@gmail.com',
  no_telp: '089789789222',
  status: 3,
  tanggal_daftar: '2021-07-01',
  no_register: 12312432,
  tahun_masuk: '2018',
  nisn: 123123123,
  nis: 123132132,
  nomor_ijazah: 123132132,
  tahun_ijazah: '2018',
  nomor_skhun: 123123132,
  tahun_skhun: '2018',
  nomor_un: 123132132,
  nik: 123132132,
  telp_rumah: '0812121245',
  kebutuhan_khusus: '',
  nama_siswa: 'Dimas Setiaji',
  nama_panggilan: 'Dimas',
  agama: 1,
  jenis_kelamin: 1,
  tempat_lahir: 'Bandung',
  tanggal_lahir: '2021-07-01',
  alamat_tinggal: 'jalan jalan',
  provinsi: 1,
  kabupaten_kota: 1,
  kecamatan: 1,
  desa: 1,
  rt: 1,
  rw: 1,
  transportasi_id: 1,
  tranportasi: 1,
  tinggal_id: 1,
  tinggal_ket: 'halo',
  nomor_kps: 12313212,
  nomor_kip: 1231213,
  nomor_kks: 12312123,
  cita_id: 1,
  cita_lainnya: 'halo everyone',
  // created_at: '',
  // updated_at: '',
  // user_siswa: {
  //   id: 25,
  //   level_id: 1,
  //   status: 3,
  //   email: '',
  //   no_telp: '',
  //   voucher: 'H9URMT3VOE',
  //   created_at: '',
  //   updated_at: '',
  //   photo: ''
  // },
  jalur: 1,
  jenis_daftar: 1,
  // kelas: 1,
  // semester: null,
  jurusan: 1
  // orang_tua: null,
  // periodik: null,
  // prestasi: [],
  // beasiswa: []
}

export default {
  getSiswa () {
    const data = {
      url: 'auth/me',
      method: 'POST'
    }
    return optionsAPI(data)
  },
  editSiswa (userData) { 
    console.log('MASUK KE SINI', dataPendaftaran)
    // Pos
    // dataPendaftaran.nama_siswa = userData.nama_lengkap
    // dataPendaftaran.nama_panggilan = userData.nama_panggilan
    // dataPendaftaran.jenis_kelamin = userData.jenis_kelamin
    // dataPendaftaran.agama = userData.agama
    // dataPendaftaran.tempat_lahir = userData.tempat_lahir
    // dataPendaftaran.tanggal_lahir = userData.tanggal_lahir
    // dataPendaftaran.kebutuhan_khusus = userData.berkebutuhan_khusus.nama
    // dataPendaftaran.nisn = userData.nisn
    // dataPendaftaran.nomor_ijazah = userData.no_izasah
    // dataPendaftaran.nomor_skhun = userData.no_skhun
    // dataPendaftaran.nomor_un = userData.no_un
    // dataPendaftaran.nik = userData.nik
    // dataPendaftaran.alamat_tinggal = userData.alamat
    // dataPendaftaran.provinsi = userData.provinsi
    // dataPendaftaran.kabupaten_kota = userData.kabupaten_kota
    // dataPendaftaran.kecamatan = userData.kecamatan
    // dataPendaftaran.desa = userData.kelurahan_desa
    // dataPendaftaran.rt = userData.rt
    // dataPendaftaran.rw = userData.rw
    // dataPendaftaran.tranportasi = userData.tranportasi_sekolah
    // dataPendaftaran.tinggal_ket = userData.jenis_tinggal.nama
    // dataPendaftaran.telp_rumah = userData.telepon
    // dataPendaftaran.nomor_kps = userData.no_kps
    // dataPendaftaran.nomor_kip = userData.no_kip
    // dataPendaftaran.nomor_kks = userData.no_kks
    // dataPendaftaran.cita_lainnya = userData.cita_cita.nama
    
    // dataPendaftaran.user_siswa.no_telp = userData.hpp
    // dataPendaftaran.user_siswa.email = userData.email
    // dataPendaftaran.user_siswa.email = userData.email
    // dataPendaftaran.prestasi = prestasi.prestasi
    // dataPendaftaran.periodik = periodik
    // dataPendaftaran.orang_tua = orangtua
    // dataPendaftaran.prestasi = prestasi.beasiswa

    const data = {
      url: 'input/siswa',
      method: 'POST',
      data: [dataPendaftaran]
    }
    return optionsAPI(data)
  },
  editProfileSiswa (userData) {
    const data = {
      url: 'input/siswa',
      method: 'POST',
      data: userData
    }
    return optionsAPI(data)
  },
  getNewSiswa () {
    const data = {
      url: 'siswa',
      method: 'GET',
      params: {
        sekolah_id: store.state.server.server.sekolah.id,
        user_id: store.state.user.user.user_siswa.id
      }
    }
    return optionsAPI(data)
  },
  uploadFotoProfile (formData) {
    const data = {
      url: 'file/profile',
      method: 'POST',
      data: formData,
      type: 'formData'
    }
    return optionsAPI(data)
  }
}
