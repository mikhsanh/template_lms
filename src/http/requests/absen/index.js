import optionsAPI from '@/http/requests/options'

const getTime = () => {
  const date = new Date()

  const day = date.getDate()
  const month = date.getMonth() + 1
  const year = date.getFullYear()

  const hours = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return `${year}-${month}-${day} ${hours}:${minute}:${second}`
}

export default {
  getAbsenGuru () {
    const data = {
      url: 'input/absen-guru',
      method: 'POST',
      data: [
        {
          guru_id: 1,
          jadwal_id: 1,
          jam_masuk: getTime(),
          keterangan: 'Guru Masuk'
        }
      ]
    }
    return optionsAPI(data)
  },
  sendAbsen (id) {
    const data = {
      url: 'input/absen-siswa',
      method: 'POST',
      data: [
        {
          siswa_id: JSON.parse(sessionStorage.getItem('user')).id,
          jadwal_id: id,
          jam_masuk: getTime(),
          keterangan: '',
          status: 1
        }
      ]
    }
    return optionsAPI(data)
  },
  uploadAbsenFile (formData) {
    const data = {
      url: 'file/absen',
      method: 'POST',
      data: formData,
      type: 'formData'
    }
    return optionsAPI(data)
  }
}
