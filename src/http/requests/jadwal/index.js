import optionsAPI from '@/http/requests/options'

const user = JSON.parse(sessionStorage.getItem('user'))

export default {
  getJadwal (mapel_id) {
    const data = {
      url: 'jadwal',
      method: 'GET',
      params: { 
        sekolah_id: user.sekolah.id,
        mapel_id
      }
    }
    return optionsAPI(data)
  },
  getJadwalAkademik () {
    const data = {
      url: 'kelender-akedemik',
      method: 'GET',
      params: { 
        sekolah_id: user.sekolah.id
      }
    }
    return optionsAPI(data)
  },
  getFileJadwal () {
    const data = {
      url: 'file-kalender',
      method: 'GET',
      params: { 
        sekolah_id: user.sekolah.id
      }
    }
    return optionsAPI(data)
  }
}
