import axios from '@/http/axios'
import { API_FIRST } from '@/auth/apiService'
import {secretKey} from '@/auth/secretKey'
import store from '@/store/store.js'

const optionsAPI = (data = {}) => {
  let headers = {
    'Authorization': `bearer ${sessionStorage.getItem('token')}`,
    'secret': secretKey
  }

  if (data.type === 'formData') {
    headers = {
      'Authorization': `bearer ${sessionStorage.getItem('token')}`,
      'secret': secretKey,
      'Content-Type': 'multipart/form-data'
    }
  }

  let optionsAPI = ''
  if (data.api === 'first') {
    optionsAPI = {
      method: data.method,
      url: API_FIRST + data.url,
      params: data.params,
      data: data.data,
      headers
    }
  } else {
    optionsAPI = {
      method: data.method,
      url: `${store.getters['server/getServer'].back_server.server}/${data.url}`,
      params: data.params,
      data: data.data,
      headers
    }
  }

  const sendAPI = new Promise((resolve, reject) => {
    axios.request(optionsAPI).then(function (response) {
      resolve(response.data)
    }).catch(function (error) {
      reject(error.response)
    })
  })

  return sendAPI
}

export default optionsAPI
