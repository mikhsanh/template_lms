import optionsAPI from '@/http/requests/options'

const user = JSON.parse(sessionStorage.getItem('user'))

export default {
  getUjian (mapel_id) {
    const data = {
      url: 'ujian',
      method: 'GET',
      params: 
        {
          sekolah_id: user.sekolah.id,
          mapel_id
        }
    }
    return optionsAPI(data)
  },
  getSoalUjian (id) {
    const data = {
      url: 'soal-ujian',
      method: 'GET',
      params: 
        {
          type_ujian: id,
          kelas_id: user.kelas.id,
          semester_id: user.semester.id,
          jurusan_id: user.jurusan.id,
          status: 2
        }
    }
    return optionsAPI(data) 
  },
  postUjian (id) {
    const data = {
      url: 'input/hasil-ujian',
      method: 'POST',
      data: [
        {
          siswa_id: user.id,
          ujian_id: id
        }
      ]
    }
    return optionsAPI(data) 
  },
  startUjian (id) {
    const data = {
      url: 'hasil-ujian',
      method: 'GET',
      data: [
        {
          siswa_id: user.id,
          ujian_id: id
        }
      ]
    }
    return optionsAPI(data) 
  },
  uploadUjian (id, ujian_id, jawaban_pg = [], jawaban_essay = [], essay_id) {
    const data = {
      url: 'input/hasil-ujian',
      method: 'POST',
      data: [
        {
          id,
          ujian_id,
          siswa_id: user.id,
          jawaban_pg,
          jawaban_essay,
          essay_id
        }
      ]
    }
    return optionsAPI(data) 
  },
  finishUjian ({id, ujian_id, jawaban_pg = [], jawaban_essay = [], essay_id}) {
    
    const data = {
      url: 'input/hasil-ujian',
      method: 'POST',
      data: [
        {
          id,
          ujian_id,
          siswa_id: user.id,
          jawaban_pg,
          jawaban_essay,
          essay_id,
          status_selesai: 1
        }
      ]
    }
    return optionsAPI(data) 
  },
  uploadUjianFile (formData) {
    const data = {
      url: 'file/jawaban-essay',
      method: 'POST',
      data: formData,
      type: 'formData'
    }
    return optionsAPI(data)
  }
}