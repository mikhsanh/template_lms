import optionsAPI from '@/http/requests/options'

const user = JSON.parse(sessionStorage.getItem('user'))

export default {
  // async getNilai (tugasId, mapelId) {
  //   const data = {
  //     url: 'tugas',
  //     method: 'GET',
  //     params:  {
  //       kelas_id: user.kelas.id,
  //       sekolah_id: user.sekolah.id
  //     }
  //   }

  //   const getTugas = await optionsAPI(data)

  //   const dataTugas = getTugas.data.map(item => {
  //     return {id: item.id, nama: item.nama_tugas}
  //   })

  //   const getNilai = dataTugas.map(async (item) => {
  //     const data = {
  //       url: 'tugas-siswa',
  //       method: 'GET',
  //       params:  {
  //         siswa_id: user.id,
  //         // mapel_id: mapelId,
  //         tugas_id: item.id
  //       }
  //     }

  //     const dataNilai = await optionsAPI(data)

  //     console.log('INI NILAI YA', dataNilai)

  //     return {...dataNilai, ...item}
  //   })

  //   console.log('APAKAH INI YANG DINAMAKAN NILAI', await getNilai)

  //   return dataTugas

  //   // return optionsAPI(data)
    
  // }
  getNilai (tugasId) {
    console.log('TUGAS ID NIH', tugasId)
    const data = {
      url: 'tugas-siswa',
      method: 'GET',
      params:  {
        siswa_id: user.id,
        tugas_id: tugasId
      }
    }

    return optionsAPI(data)
  }
}
