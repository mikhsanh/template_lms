import optionsAPI from '@/http/requests/options'

const user = JSON.parse(sessionStorage.getItem('user'))

export default {
  getMateri (mapel_id) {
    const data = {
      url: 'materi',
      method: 'GET',
      params:  {
        sekolah_id: user.sekolah.id,
        mapel_id
      }
    }
    return optionsAPI(data)
  }
}
