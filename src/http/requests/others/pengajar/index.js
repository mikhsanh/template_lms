import optionsAPI from '@/http/requests/options'

const user = JSON.parse(sessionStorage.getItem('user'))

export default {
  getPengajar () {
    const data = {
      url: 'pengajar',
      method: 'GET',
      params: {
        kelas_id: user.kelas_id,
        semester_id: user.semester.id,
        jurusan_id: user.jurusan.id
      }
    }
    return optionsAPI(data)
  },
  getMapelId () {
    const data = {
      url: 'pengajar',
      method: 'GET',
      params: {
        kelas_id: user.kelas_id,
        semester_id: user.semester.id,
        jurusan_id: user.jurusan.id
      }
    }
    
    return optionsAPI(data).then(response => {
      const dataPengajar = response.data
      let mapelId = ''
      let dataMapelId = dataPengajar.map((mapels) => {
        const {mapel} = mapels
        return mapel.id
      })
      dataMapelId = [...new Set(dataMapelId)]
      dataMapelId.map((item) => {
        mapelId += `${item},`
      })
      return mapelId
    }).catch(err => {
      return {
        status: 'error',
        message: err
      }
    })
  }
}
