import optionsAPI from '@/http/requests/options'

const user = JSON.parse(sessionStorage.getItem('user'))

export default {
  getJurusan () {
    const data = {
      url: 'jurusan',
      method: 'GET',
      params: {
        sekolah_id: user.sekolah.id
      }
    }
    return optionsAPI(data)
  }
}
