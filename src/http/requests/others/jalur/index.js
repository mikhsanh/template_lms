import optionsAPI from '@/http/requests/options'

const user = JSON.parse(sessionStorage.getItem('user'))

export default {
  getJalur () {
    const data = {
      url: 'jalur',
      method: 'GET',
      params: {
        sekolah_id: user.sekolah.id
      }
    }
    return optionsAPI(data)
  }
}
