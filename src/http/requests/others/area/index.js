import optionsAPI from '@/http/requests/options'

export default {
  getProvince () {
    const data = {
      url: 'provinsi',
      method: 'GET'
    }
    return optionsAPI(data)
  },
  getDistrict (id) {
    const data = {
      url: 'kabupaten-kota',
      method: 'GET',
      params: {
        provinsi_id: id 
      }
    }
    return optionsAPI(data)
  },
  getSubDistrict (id) {
    const data = {
      url: 'kecamatan',
      method: 'GET',
      params: {
        kabupaten_id: id 
      }
    }
    return optionsAPI(data)
  },
  getVillage (id) {
    const data = {
      url: 'desa',
      method: 'GET',
      params: {
        kecamatan_id: id 
      }
    }
    return optionsAPI(data)
  }
}