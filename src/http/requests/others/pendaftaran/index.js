import optionsAPI from '@/http/requests/options'

export default {
  sendPendaftaran (dataPendaftaran) {
    const data = {
      api: 'first',
      url: 'auth/daftar',
      method: 'POST',
      data:  [dataPendaftaran]
    }
    return optionsAPI(data)
  }
}
