import optionsAPI from '@/http/requests/options'

export default {
  getGelombang (id) {
    const data = {
      api: 'first',
      url: 'auth/gelombang/daftar',
      method: 'GET',
      params:  {
        sekolah_id: id
      }
    }
    return optionsAPI(data)
  }
}
