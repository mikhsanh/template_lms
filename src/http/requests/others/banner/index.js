import optionsAPI from '@/http/requests/options'

export default {
  getBanner () {
    const data = {
      url: 'banner-global',
      method: 'GET',
      params: {
        provinsi_id: 2
      }
    }
    return optionsAPI(data)
  }
}
