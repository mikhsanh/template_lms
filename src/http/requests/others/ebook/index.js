import optionsAPI from '@/http/requests/options'

export default {
  getEbook () {
    const data = {
      url: 'ebook',
      method: 'GET'
    }
    return optionsAPI(data)
  }
}
