import optionsAPI from '@/http/requests/options'

export default {
  getMapel () {
    const data = {
      url: 'mapel',
      method: 'GET',
      params: {
        kelas_id: JSON.parse(sessionStorage.getItem('user')).kelas_id
      }
    }
    return optionsAPI(data)
  }
}
