import optionsAPI from '@/http/requests/options'

const user = JSON.parse(sessionStorage.getItem('user'))

export default {
  getTugas (mapel_id) {
    const data = {
      url: 'tugas',
      method: 'GET',
      params: {
        kelas_id: user.kelas.id,
        sekolah_id: user.sekolah.id,
        mapel_id
      }
    }
    return optionsAPI(data)
  },
  getTugasSiswa (tugas_id) {
    const data = {
      url: 'tugas-siswa',
      method: 'GET',
      params: {
        siswa_id: user.id,
        tugas_id
      }
    }
    return optionsAPI(data)
  },
  uploadTugas (id) {
    const data = {
      url: 'input/tugas-siswa',
      method: 'POST',
      data: [
        {
          tugas_id: id,
          siswa_id: user.id,
          nilai: 0
        }
      ]
    }
    return optionsAPI(data)
  },
  uploadTugasFile (formData) {
    const data = {
      url: 'file/tugas-siswa',
      method: 'POST',
      data: formData,
      type: 'formData'
    }
    return optionsAPI(data)
  }
}
