export default {
  setDataUser ({commit}, data) {
    commit('SET_DATA_USER', data)
  }
}