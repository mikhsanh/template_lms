export default {
  SET_DATA_PILIHAN_GANDA (state, data) {
    state.dataPilihanGanda = data
  },
  SET_DATA_ESSAY (state, data) {
    state.dataEssay = data
  },
  SET_DATA_UJIAN (state, data) {
    state.dataUjian = data
  },
  SET_JAWABAN_PILIHAN_GANDA (state, data) {
    state.jawabanPilihanGanda = data
  },
  SET_CURRENT_PG (state, data) {
    state.currentPg = data
  },
  SET_CURRENT_ESSAY (state, data) {
    state.currentEssay = data
  },
  SET_IS_PG (state, data) {
    state.isPg = data
  },
  SET_IS_ESSAY (state, data) {
    state.isEssay = data
  },
  SET_NOW_UJIAN (state, data) {
    state.nowUjian = data
  }
}