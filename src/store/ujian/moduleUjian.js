import state from './moduleUjianState.js'
import mutations from './moduleUjianMutations.js'
import actions from './moduleUjianActions.js'
import getters from './moduleUjianGetters.js'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}