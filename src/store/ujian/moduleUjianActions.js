export default {
  setDataPilihanGanda ({commit}, data) {
    commit('SET_DATA_PILIHAN_GANDA', data)
  },
  setDataEssay ({commit}, data) {
    commit('SET_DATA_ESSAY', data)
  },
  setDataUjian ({commit}, data) {
    commit('SET_DATA_UJIAN', data)
  },
  setJawabanPilihanGanda ({commit}, data) {
    commit('SET_JAWABAN_PILIHAN_GANDA', data)
  },
  setCurrentPg ({commit}, data) {
    commit('SET_CURRENT_PG', data)
  },
  setCurrentEssay ({commit}, data) {
    commit('SET_CURRENT_ESSAY', data)
  },
  setIsPg ({commit}, data) {
    commit('SET_IS_PG', data)
  },
  setIsEssay ({commit}, data) {
    commit('SET_IS_ESSAY', data)
  },
  setNowUjian ({commit}, data) {
    commit('SET_NOW_UJIAN', data)
  }
}