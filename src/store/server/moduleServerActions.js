export default {
  setDataServer ({commit}, data) {
    commit('SET_DATA_SERVER', data)
  }
}