import state from './moduleServerState.js'
import mutations from './moduleServerMutations.js'
import actions from './moduleServerActions.js'
import getters from './moduleServerGetters.js'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}