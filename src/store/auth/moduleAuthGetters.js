export default {
  emptyToken: state => state.token === null && localStorage.token === null,
  userProfile: state => {
    const userInfo = state.userInfo === undefined ? localStorage.userInfo : state.userInfo
    const user = JSON.parse(userInfo),
      profile = {
        name: user.profile[0].siswa.nama_siswa,
        email: user.profile[0].email,
        photo: user.profile[0].photo === null ? 'portrait/small/avatar-s-1.jpg' : user.profile[0].photo,
        noPhoto: user.profile[0].photo === null,
        level: user.profile[0].level_id.nama_level
      }

    return profile
  }
}
